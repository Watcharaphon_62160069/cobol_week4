       IDENTIFICATION DIVISION. 
       PROGRAM-ID. CobolBMI.
       AUTHOR. WATCHARAPHON.

       DATA DIVISION. 
       WORKING-STORAGE SECTION. 
       01  WEIGHT         PIC 999V9  VALUE ZERO.
       01  HEIGHT         PIC 999V9  VALUE ZERO.
       01  HEIGHT-M       PIC 999V9  VALUE ZERO.

       01  BMI            PIC 999V9.
           88  LEVEL-UNDERWEIGHT     VALUE 0.0  THRU 18.4.
           88  LEVEL-NORMAL          VALUE 18.5 THRU 24.9.
           88  LEVEL-OVERWEIGHT      VALUE 25.0 THRU 29.9.
           88  LEVEL-OBESE           VALUE 30.0 THRU 34.9.
           88  LEVEL-EXTREMLY-OBESE  VALUE 35.0 THRU 99.0.

       01  BMI-MESSAGE    PIC X(30)  VALUE SPACE.
           88  BMI-UNDERWEIGHT       VALUE "< 18.5 : UNDERWEIGHT".
           88  BMI-NORMAL            VALUE "18.5 - 24.9 : NORMAL".
           88  BMI-OVERWEIGHT        VALUE "25.0 - 29.9 : OVERWEIGHT ".
           88  BMI-OBESE             VALUE "30.0 - 34.9 : OBESE ".
           88  BMI-EXTREMLY-OBESE    VALUE "35 < : EXTREMLY OBESE".
           
       PROCEDURE DIVISION .
       BEGIN.
           DISPLAY "ENTER WEIGHT (KG) : " WITH NO ADVANCING 
           ACCEPT WEIGHT
           DISPLAY "ENTER HEIGHT (CM) : " WITH NO ADVANCING 
           ACCEPT HEIGHT

           DIVIDE HEIGHT BY 100 GIVING HEIGHT-M 
           COMPUTE BMI = WEIGHT / ( HEIGHT-M * HEIGHT-M )
           DISPLAY "BMI : " BMI

           EVALUATE TRUE 
              WHEN  LEVEL-UNDERWEIGHT    SET BMI-UNDERWEIGHT     TO TRUE
              WHEN  LEVEL-NORMAL         SET BMI-NORMAL          TO TRUE
              WHEN  LEVEL-OVERWEIGHT     SET BMI-OVERWEIGHT      TO TRUE
              WHEN  LEVEL-OBESE          SET BMI-OBESE           TO TRUE
              WHEN  LEVEL-EXTREMLY-OBESE SET BMI-EXTREMLY-OBESE  TO TRUE
           END-EVALUATE 
           
           
           DISPLAY BMI-MESSAGE
           
           GOBACK 
           .

           